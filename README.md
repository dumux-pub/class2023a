# Class2023a

Welcome to the dumux pub module for Class2023a
======================================================

This module contains the source code for the paper:

Class, H., Keim, L., Schirmer, L., Strauch, B., Wendel, K., Zimmer, M. (2023). [Seasonal Dynamics of Gaseous CO2 Concentrations in a Karst Cave Correspond With Aqueous Concentrations in a Stagnant Water Column]



To install this module and its dependencies create a new
directory and clone this module:

```
mkdir New_Folder && cd New_folder
git clone https://git.iws.uni-stuttgart.de/dumux-pub/class2023a.git
```

After that, execute the file [installscript.sh](https://git.iws.uni-stuttgart.de/dumux-pub/class2023a/-/blob/main/installscript.sh)

```
chmod +x class2023a/installscript.sh
./class2023a/installscript.sh
```

This should automatically download all necessary modules and check out the correct versions.

Finally, run

```
./dune-common/bin/dunecontrol --opts=dumux/optim.opts all
```


Applications
============

To run the main simulations go to the following folder and compile the programs
```
cd class2023a/build-cmake/appl/isothermal/column
make column_isothermal
make column_isothermal_superdiffusion
make column_isothermal_pulses
```

Then run the main simulations

```
./column_isothermal
./column_isothermal_superdiffusion
./column_isothermal_pulses
```

There are also several smaller tests discussed in the paper.
columntop_constant can be used for reproduction of the grid study with use of constant boundary conditions
columntop_usedata is the same with exception that data from the measurements is used for the boundary conditions
smallcolumn_isothermal and smallcolumn3D_isothermal resemble a test on a small domain to compare a 2D and a 3D setup.

In analogy to the main simulations, go to the following folders, compile the programs and run them

```
cd class2023a/build-cmake/test/isothermal/columntop_constant
make columntop_constant
./columntop_constant
```

resp.

```
cd class2023a/build-cmake/test/isothermal/columntop_usedata
make columtop_usedata
./columtop_usedata
```

resp.

```
cd class2023a/build-cmake/test/isothermal/smallcolumn
make smallcolumn_isothermal
./smallcolumn
```

resp.

```
cd class2023a/build-cmake/test/isothermal/smallcolumn3D
make smallcolumn3D_isothermal
./smallcolumn3D_isothermal
```
