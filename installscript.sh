# dune-common
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git reset --hard 2d84eba7333181d117e9602f5cdbc28c558e3af0
cd ..

# dune-geometry
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git reset --hard 585caabf5946b26641d50c1b343f6e183507c512
cd ..

# dune-grid
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git reset --hard bc26c645478130a6033f49fe7c9a9fc4ed5fcd3f
cd ..

# dune-localfunctions
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git reset --hard 2edfb9cab755989fa38362d268076c87a345d6c8
cd ..

# dune-istl
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git reset --hard d5eba453ec4e97696581a363a3c30a092b871515
cd ..

# dumux
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git reset --hard 79d3057e3ee504ab40d3bc918931ca83fa008481
cd ..

#class2023a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/class2023a.git

./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
