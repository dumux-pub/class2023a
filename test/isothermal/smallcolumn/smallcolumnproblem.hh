// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Application where CO2 dissolves into water and creates density driven flow
 */
#ifndef DUMUX_DENSITY_FLOW_NC_TEST_PROBLEM_HH
#define DUMUX_DENSITY_FLOW_NC_TEST_PROBLEM_HH


namespace Dumux
{


/*!
 * \brief Application where CO2 dissolves into water and creates density driven flow
 *
 */
template <class TypeTag>
class DensityDrivenFlowProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    static constexpr auto transportCompIdx = Indices::conti0EqIdx + 1;
    static constexpr auto transportEqIdx = Indices::conti0EqIdx + 1;

    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    //using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;

    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using NumEqVector = Dumux::NumEqVector<GetPropType<TypeTag, Properties::PrimaryVariables>>;

public:
    DensityDrivenFlowProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry), eps_(1e-6),  timeIndex_(0)
    {
        initialCO2aq_ = getParam<Scalar>("Problem.initialCO2aq");
        temperatureInitial_ = getParam<Scalar>("Problem.Temperature")+273.15;
        restartTime_ = getParam<Scalar>("TimeLoop.Restart",0.0);

        FluidSystem::init();
        deltaRho_.resize(this->gridGeometry().numCellCenterDofs());

        // Adding also the domain height to the naming:
        const std::string heightStr = getParam<std::string>("Grid.Positions1");

        filenameTotalMolesCO2_ = "totalMolesCO2"+getParam<std::string>("Output.TestType", "")
        + "_H" + heightStr.substr(2,heightStr.size()) +"m"
        +".log";
        fileTotalMoles_.open(filenameTotalMolesCO2_);
        fileTotalMoles_<< "time totalMolesCO2 totalMoles totalKineticEnergy" << std::endl;
        name_ = getParam<std::string>("Problem.Name") +getParam<std::string>("Output.TestType", "")
        + "_H" + heightStr.substr(2,heightStr.size()) +"m";
        fileTotalMoles_.close();
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        // set Dirichlet values for the velocity everywhere
        values.setDirichlet(Indices::velocityXIdx);
        values.setDirichlet(Indices::velocityYIdx);

        if (onTopBoundary_(globalPos))
        {
            values.setDirichlet(transportEqIdx);
        }

        else
        {
            values.setNeumann(Indices::conti0EqIdx);
            values.setNeumann(transportEqIdx);
        }
        return values;
    }

    /*!
     * \brief Returns whether a fixed Dirichlet value shall be used at a given cell.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scv The sub control volume
     */
    template<class FVElementGeometry, class SubControlVolume>
    bool isDirichletCell(const Element& element,
                         const FVElementGeometry& fvGeometry,
                         const SubControlVolume& scv,
                         int pvIdx) const
    {
        return (isLowerLeftCell_(scv) && (pvIdx == Indices::pressureIdx ));
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values = initialAtPos(globalPos);
        return values;
    }

    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub-control volume face.
     */
    using ParentType::source;
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const ElementFaceVariables& elemFaceVars,
                       const SubControlVolumeFace& scvf) const
    {
        auto source = NumEqVector(0.0);

        return source;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;

        const Scalar hydroStatPress=(this->gridGeometry().bBoxMax()[dimWorld-1]-globalPos[dimWorld-1])*9.81*1000;

        values[Indices::pressureIdx] = atmPressure+ hydroStatPress;

        if(onTopBoundary_(globalPos))
        {
            values[transportCompIdx] = 0.5e-6*std::sin(2.424068*time())+7.5e-6;
        }
        else
        {
            values[transportCompIdx] = initialCO2aq_;
        }

        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;

        return values;
    }

    void setTimeLoop(TimeLoopPtr timeLoop)
    {
        timeLoop_ = timeLoop;
    }

    template<class GridVariables, class SolutionVector>
    std::vector<std::pair<std::string, Scalar>> getPostProcessingData(const GridVariables& gridVars,
                                 const SolutionVector& x) const
    {
        using DataPair = std::pair<std::string, Scalar>;
        using Result = std::vector<DataPair>;
        Result result;
        int i = 0;
        int idx_xCO20_85m     = i++;       result.emplace_back( std::make_pair("xCO2_upper",0.0) );
        int idx_xCO20_5m     = i++;       result.emplace_back( std::make_pair("xCO2_middle",0.0) );
        int idx_xCO20_15m    = i++;       result.emplace_back( std::make_pair("xCO2_lower",0.0) );
        Scalar VolWeighted0_85m = 0.0;
        Scalar VolWeighted0_5m = 0.0;
        Scalar VolWeighted0_15m = 0.0;

            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());

            for ( i = 0; i < upperProbeRegionElmIdces_.size(); i++)
            {
                const auto& element = this->gridGeometry().element(upperProbeRegionElmIdces_[i]);
                fvGeometry.bind(element);
                elemVolVars.bind(element, fvGeometry, x);
                for (const auto& scv : scvs(fvGeometry))
                {
                    VolWeighted0_85m += elemVolVars[scv].moleFraction(0,1)*elemVolVars[scv].extrusionFactor()*scv.volume();
                }
            }
        result[idx_xCO20_85m].second = VolWeighted0_85m/avgVol_0_85m_;

            for ( i = 0; i < middleProbleRegionElmIdces_.size(); i++)
            {
                const auto& element = this->gridGeometry().element(middleProbleRegionElmIdces_[i]);
                fvGeometry.bind(element);
                elemVolVars.bind(element, fvGeometry, x);
                for (const auto& scv : scvs(fvGeometry))
                {
                    VolWeighted0_5m += elemVolVars[scv].moleFraction(0,1)*elemVolVars[scv].extrusionFactor()*scv.volume();
                }
            }
        result[idx_xCO20_5m].second = VolWeighted0_5m/avgVol_0_5m_;

            for ( i = 0; i < lowerProbeRegionElmIdces_.size(); i++)
            {
                const auto& element = this->gridGeometry().element(lowerProbeRegionElmIdces_[i]);
                fvGeometry.bind(element);
                elemVolVars.bind(element, fvGeometry, x);
                for (const auto& scv : scvs(fvGeometry))
                {
                    VolWeighted0_15m += elemVolVars[scv].moleFraction(0,1)*elemVolVars[scv].extrusionFactor()*scv.volume();
                }
            }
        result[idx_xCO20_15m].second = VolWeighted0_15m/avgVol_0_15m_;

        return result;
    }

    void calculateAvgVols(const GridVariables& gridVars,
                                 const SolutionVector& x)
    {
        for( const auto& element : elements(this->gridGeometry().gridView()) )
        {
            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            const auto& globalPos = element.geometry().center();
            const unsigned int elemIdx = this->gridGeometry().elementMapper().index(element);

            const Scalar yUpperProbe = 0.85*this->gridGeometry().bBoxMax()[1];
            const Scalar yMiddleProbe = 0.5*this->gridGeometry().bBoxMax()[1];
            const Scalar yLowerProbe = 0.15*this->gridGeometry().bBoxMax()[1];

            if(globalPos[0] < 0.06 && globalPos[0] > 0.04)
            {
                if(globalPos[1] > yLowerProbe-0.01 && globalPos[1] < yLowerProbe)
                {
                    lowerProbeRegionElmIdces_.push_back(elemIdx);
                    for (const auto& scv : scvs(fvGeometry))
                    {
                        avgVol_0_15m_ += elemVolVars[scv].extrusionFactor()*scv.volume();
                        std::cout << "Submit volume to region of lower Probe. ElemIdx : " << elemIdx << " which adds "<< elemVolVars[scv].extrusionFactor()*scv.volume() << " cubic meters per meter"<<std::endl;
                    }
                }
            }

            if(globalPos[0] < 0.06 && globalPos[0] > 0.04)
            {
                if(globalPos[1] > yMiddleProbe-0.01 && globalPos[1] < yMiddleProbe)
                {
                    middleProbleRegionElmIdces_.push_back(elemIdx);
                    for (const auto& scv : scvs(fvGeometry))
                    {
                        avgVol_0_5m_ += elemVolVars[scv].extrusionFactor()*scv.volume();
                        std::cout << "Submit volume to region of middle Probe. ElemIdx : " << elemIdx << " which adds "<< elemVolVars[scv].extrusionFactor()*scv.volume() << " cubic meters per meter"<<std::endl;
                    }
                }
            }

            if(globalPos[0] < 0.06 && globalPos[0] > 0.04)
            {
                if(globalPos[1] > yUpperProbe-0.01 && globalPos[1] <yUpperProbe)
                {
                    upperProbeRegionElmIdces_.push_back(elemIdx);
                    for (const auto& scv : scvs(fvGeometry))
                    {
                        avgVol_0_85m_ += elemVolVars[scv].extrusionFactor()*scv.volume();
                        std::cout << "Submit volume to region of upper Probe. ElemIdx : " << elemIdx << " which adds "<< elemVolVars[scv].extrusionFactor()*scv.volume() << " cubic meters per meter"<<std::endl;
                    }
                }
            }

        }
    }
    void calculateTotalCO2Moles(const GridVariables& gridVariables, const SolutionVector& sol)
    {
        Scalar totalMolesCO2 = 0.0;
        Scalar totalMoles = 0.0;
        Scalar totalKineticEnergy=0.0;
        GlobalPosition velocity(0.0);

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
                auto fvGeometry = localView(this->gridGeometry());
                fvGeometry.bind(element);
                for (auto&& scv : scvs(fvGeometry))
                {
                    auto dofIdxGlobal = scv.dofIndex();
                    auto elemVolVars = localView(gridVariables.curGridVolVars());
                    elemVolVars.bind(element, fvGeometry, sol);
                    const auto& volVars = elemVolVars[scv];

                    totalMolesCO2 += volVars.molarDensity() * volVars.moleFraction(0, 1) * scv.volume();

                    totalMoles += volVars.molarDensity() * scv.volume();
                    // calculate velocities
                    GlobalPosition velocityTemp(0.0);
                    for (auto&& scvf : scvfs(fvGeometry))
                    {
                        const int dofIdxFace = scvf.dofIndex();
                        const auto numericalSolutionFace = sol[GridGeometry::faceIdx()][dofIdxFace][Indices::velocity(scvf.directionIndex())];
                        velocityTemp[scvf.directionIndex()] += 0.5*numericalSolutionFace;
                    }
                    const Scalar velMag = velocityTemp.two_norm();
                    totalKineticEnergy += 0.5*volVars.density() * velMag *elemVolVars[scv].extrusionFactor()*scv.volume();
                }
        }

        fileTotalMoles_.open(filenameTotalMolesCO2_, std::ios::app);
        fileTotalMoles_<< std::scientific << time() << " " << totalMolesCO2 << " " << totalMoles << " " <<  totalKineticEnergy<< std::endl;
        fileTotalMoles_.close();
    }
    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    void calculateDeltaRho(const GridVariables& gridVariables, const SolutionVector& sol)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);
            for (auto&& scv : scvs(fvGeometry))
            {
                auto ccDofIdx = scv.dofIndex();

                auto elemVolVars = localView(gridVariables.curGridVolVars());
                elemVolVars.bind(element, fvGeometry, sol);

                deltaRho_[ccDofIdx] = elemVolVars[scv].density() - 999.694;
            }
        }
    }

    auto& getDeltaRho() const
    { return deltaRho_; }

    // \}
    template<class SubControlVolume>
    bool isLowerLeftCell_(const SubControlVolume& scv) const
    {
        return scv.dofIndex() == 0;
    }
    const std::string& name() const
    {
        return name_;
    }
private:

    bool onTopBoundary_(const GlobalPosition& globalPos) const
    { return globalPos[dimWorld-1] > this->gridGeometry().bBoxMax()[dimWorld-1] - eps_; }

    Scalar time() const
    {
        return timeLoop_->time();
    }

    const Scalar eps_;
    std::string name_;
    std::vector<Scalar> deltaRho_;
    Scalar initialCO2aq_;
    TimeLoopPtr timeLoop_;
    Scalar temperatureInitial_;
    std::ofstream fileTotalMoles_;
    Scalar restartTime_;
    std::string filenameTotalMolesCO2_;
    mutable std::vector<unsigned int> upperProbeRegionElmIdces_, middleProbleRegionElmIdces_, lowerProbeRegionElmIdces_;
    Scalar avgVol_0_85m_=0;
    Scalar avgVol_0_5m_=0;
    Scalar avgVol_0_15m_=0;
    int timeIndex_;
    static constexpr Scalar atmPressure = 1e5;
};


} //end namespace

#endif
