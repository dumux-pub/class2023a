// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Application where CO2 dissolves into water and creates density driven flow
 */
#ifndef DUMUX_DENSITY_FLOW_NC_TEST_PROPERTIES_HH
#define DUMUX_DENSITY_FLOW_NC_TEST_PROPERTIES_HH

#include <vector>
#include <dune/grid/yaspgrid.hh>
#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/freeflow/compositional/navierstokesncmodel.hh>
#include <modulerelated/material/components/co2tables.hh>
#include <dumux/material/fluidsystems/brineco2.hh>
#include <dumux/material/fluidsystems/1padapter.hh>
#include "smallcolumnproblem.hh"
#include "smallcolumnspatialparams.hh"
namespace Dumux
{

namespace Properties
{

namespace TTag {
struct DensityDrivenFlowProblem { using InheritsFrom = std::tuple<NavierStokesNC, StaggeredFreeFlowModel>; };

}

// Specialize the fluid system type for this type tag
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DensityDrivenFlowProblem>
{
    using Scalar = GetPropType<TypeTag, Scalar>;
    using BrineCO2 = FluidSystems::BrineCO2<Scalar,
                                            HeterogeneousCO2Tables::CO2Tables,
                                            Components::TabulatedComponent<Components::H2O<Scalar>>,
                                            FluidSystems::BrineCO2DefaultPolicy</*constantSalinity=*/true, /*simpleButFast=*/true>>;
    static constexpr int phaseIdx = BrineCO2::liquidPhaseIdx;
    using type = FluidSystems::OnePAdapter<BrineCO2, phaseIdx>;
};
// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::DensityDrivenFlowProblem>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FlowModelSpatialParams<GridGeometry, Scalar>;
};
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr auto value = 5;/*do not consider total mass balance*/ };

template<class TypeTag>
struct UseMoles<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr bool value = true; };


// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DensityDrivenFlowProblem>{ using type = Dune::YaspGrid<2, Dune::TensorProductCoordinates<double, 2> >; };
//{
//#if THREED
//    static constexpr auto dim = 3;
//#else
//    static constexpr auto dim = 2;
//#endif
//    using type = Dune::YaspGrid<dim>;
//};

// Set the grid type
template<class TypeTag>
struct Problem<TypeTag, TTag::DensityDrivenFlowProblem>
{
    using type = Dumux::DensityDrivenFlowProblem<TypeTag>;
};

}


} //end namespace

#endif
