add_input_file_links()

dumux_add_test(NAME columntop_usedata
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMMAND ./columntop_usedata
              CMD_ARGS params.input)

set(CMAKE_BUILD_TYPE Debug)
