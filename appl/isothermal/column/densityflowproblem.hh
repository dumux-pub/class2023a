// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Application where CO2 dissolves into water and creates density driven flow
 */
#ifndef DUMUX_DENSITY_FLOW_NC_TEST_PROBLEM_HH
#define DUMUX_DENSITY_FLOW_NC_TEST_PROBLEM_HH

namespace Dumux
{

/*!
 * \brief Application where CO2 dissolves into water and creates density driven flow
 */
template <class TypeTag>
class DensityDrivenFlowProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    static constexpr auto transportCompIdx = Indices::conti0EqIdx + 1;
    static constexpr auto transportEqIdx = Indices::conti0EqIdx + 1;

    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;

    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;

    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using NumEqVector = Dumux::NumEqVector<GetPropType<TypeTag, Properties::PrimaryVariables>>;

public:
    DensityDrivenFlowProblem(std::shared_ptr<const GridGeometry> gridGeometry, const std::vector<Dune::FieldVector<double, 3>>& data)
    : ParentType(gridGeometry), eps_(1e-6), storage_(0.0), co2Data_(data), timeIndex_(0)
    {
        initialCO2aq_ = getParam<Scalar>("Problem.initialCO2aq");
        temperatureInitial_ = getParam<Scalar>("Problem.Temperature")+273.15;
        intitializationTime_ = getParam<Scalar>("Problem.IntitializationTime", 0.0);
        restartTime_ = getParam<Scalar>("TimeLoop.Restart",0.0);

        FluidSystem::init();
        deltaRho_.resize(this->gridGeometry().numCellCenterDofs());
        actualData_[0]=co2Data_[0][1]; // CO2
        actualData_[1]=co2Data_[0][2]; // Pressure

#if TOP_BOUNDARY_ARTIFICIAL_SUPERDIFFUSION==1
    filenameTotalMolesCO2_ = "totalMolesCO2_Superdiffusion"+getParam<std::string>("Output.TestType", "") + ".log";
#elif PULSES==1
    filenameTotalMolesCO2_ = "totalMolesCO2_Pulses"+getParam<std::string>("Output.TestType", "") + ".log";
#else
    filenameTotalMolesCO2_ = "totalMolesCO2_Normal"+getParam<std::string>("Output.TestType", "") + ".log";
#endif
        fileTotalMoles_.open(filenameTotalMolesCO2_);
        fileTotalMoles_<< "time totalMolesCO2 totalMoles" << std::endl;
        name_ = getParam<std::string>("Problem.Name") +getParam<std::string>("Output.TestType", "")
#if TOP_BOUNDARY_ARTIFICIAL_SUPERDIFFUSION==1
    +"_Superdiffusion";
#elif PULSES==1
    +"_Pulses";
#else
    +"_Normal";
#endif
        fileTotalMoles_.close();

#if PULSES==1
        assignPerturbationElements_();
#endif
    }

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        // set Dirichlet values for the velocity everywhere
        values.setDirichlet(Indices::velocityXIdx);
        values.setDirichlet(Indices::velocityYIdx);

        if (onTopBoundary_(globalPos))
        {
            values.setDirichlet(transportEqIdx);
        }

        else
        {
            values.setNeumann(Indices::conti0EqIdx);
            values.setNeumann(transportEqIdx);
        }
        return values;
    }

    /*!
     * \brief Returns whether a fixed Dirichlet value shall be used at a given cell.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scv The sub control volume
     */
    template<class FVElementGeometry, class SubControlVolume>
    bool isDirichletCell(const Element& element,
                         const FVElementGeometry& fvGeometry,
                         const SubControlVolume& scv,
                         int pvIdx) const
    {
        return (isLowerLeftCell_(scv) && pvIdx == Indices::pressureIdx);
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values = initialAtPos(globalPos);
        auto actualTime = time() + timeLoop_->timeStepSize();
        if(actualTime > intitializationTime_)
        {
            if (onTopBoundary_(globalPos))
            {
                if(co2Data_.size() <= timeIndex_ + 1)
                    DUNE_THROW(Dune::InvalidStateException, "Time index exceeds number of data points");

                values[transportCompIdx] = actualData_[0];
            }
        }
        return values;

    }

    void correctTimeIndex(unsigned int val)
    {
        timeIndex_=val;
        std::cout << "timeIndex_ has been set to "<<timeIndex_<<std::endl;
        std::cout << "currentTime is "<< timeLoop_->time() << " and co2Data_[timeIndex_][0]=" << co2Data_[timeIndex_][0] << " if they are not equal, please check your params.input file!"<<std::endl;
        actualizeInput();
    }
    void actualizeInput()
    {
        auto actualTime = time() + timeLoop_->timeStepSize();
        auto& prevData = co2Data_[timeIndex_];
        auto& nextData = co2Data_[timeIndex_+1];

        actualData_[0] = (prevData[1] *(nextData[0] - actualTime) +  nextData[1] *(actualTime - prevData[0]))/(nextData[0] - prevData[0]);
        actualData_[1] = (prevData[2]*(nextData[0] - actualTime) + nextData[2] *(actualTime - prevData[0]))/(nextData[0] - prevData[0]);
    }
    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub-control volume face.
     */
    using ParentType::source;
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const ElementFaceVariables& elemFaceVars,
                       const SubControlVolumeFace& scvf) const
    {
        auto source = NumEqVector(0.0);
#if PULSES==1
        if(perturb_)
        {
            const auto& eIdx = this->gridGeometry().elementMapper().index(element);
            if(perturbativeElements_[eIdx])
            {
                const auto& xMiddle = this->gridGeometry().bBoxMax()[0]/2.0;
                const auto& globalPos = element.geometry().center();
                if(globalPos[0]<xMiddle)
                {
                    source[scvf.directionIndex()] = 0.005;
                }
                else
                {
                    source[scvf.directionIndex()] = -0.005;
                }
            }
        }
#endif
        return source;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        const Scalar hydroStatPress=(this->gridGeometry().bBoxMax()[dimWorld-1]-globalPos[dimWorld-1])*9.81*1000;

        values[Indices::pressureIdx] = actualData_[1]+ hydroStatPress;
        values[transportCompIdx] = initialCO2aq_;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;

        return values;
    }

    void setTimeLoop(TimeLoopPtr timeLoop)
    {
        timeLoop_ = timeLoop;
    }

    void updateTimeIndex()
    {
        if(co2Data_.size() <= timeIndex_ + 1)
            DUNE_THROW(Dune::InvalidStateException, "Time index exceeds number of data points");

        auto nextData = co2Data_[timeIndex_+1];

        if(nextData[0] < time() + 1e-2)
        {
            timeIndex_++;
#if PULSES==1
            tIdxCount_++;
            std::cout << "tIdxCount_ = " << tIdxCount_ << " timeIndex_ = " << timeIndex_ << std::endl;
            if(tIdxCount_==24)
            {
                std::cout<<"switch on perturbation for 1h"<<std::endl;
                perturb_ = true;
                tIdxCount_=0;
            }
            else
            {
                std::cout <<"switch off perturbations."<<std::endl;
                perturb_ = false;
            }
#endif
        }
    }

    void calculateTotalCO2Moles(const GridVariables& gridVariables, const SolutionVector& sol)
    {
        Scalar totalMolesCO2 = 0.0;
        Scalar totalMoles = 0.0;

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
                auto fvGeometry = localView(this->gridGeometry());
                fvGeometry.bind(element);
                for (auto&& scv : scvs(fvGeometry))
                {
                    auto elemVolVars = localView(gridVariables.curGridVolVars());
                    elemVolVars.bind(element, fvGeometry, sol);

                    const auto& volVars = elemVolVars[scv];
                    totalMolesCO2 += volVars.molarDensity() * volVars.moleFraction(0, 1) * scv.volume();
                    totalMoles += volVars.molarDensity() * scv.volume();
                }
        }

        fileTotalMoles_.open(filenameTotalMolesCO2_, std::ios::app);
        fileTotalMoles_<< std::scientific << time() << " " << totalMolesCO2 << " " << totalMoles << std::endl;
        fileTotalMoles_.close();
    }
    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    void calculateDeltaRho(const GridVariables& gridVariables, const SolutionVector& sol)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);
            for (auto&& scv : scvs(fvGeometry))
            {
                auto ccDofIdx = scv.dofIndex();

                auto elemVolVars = localView(gridVariables.curGridVolVars());
                elemVolVars.bind(element, fvGeometry, sol);

                deltaRho_[ccDofIdx] = elemVolVars[scv].density() - 999.694;
            }
        }
    }

    auto& getDeltaRho() const
    { return deltaRho_; }

    // \}
    template<class GridVariables, class SolutionVector>
    std::vector<std::pair<std::string, Scalar>> getPostProcessingData(const GridVariables& gridVars,
                                 const SolutionVector& x) const
    {
        using DataPair = std::pair<std::string, Scalar>;
        using Result = std::vector<DataPair>;
        Result result;
        int i = 0;
        int idx_xCO2liq_5m    = i++;       result.emplace_back( std::make_pair("xCO2liq_5m",0.0) );
        int idx_xCO2liq_3m    = i++;       result.emplace_back( std::make_pair("xCO2liq_3m",0.0) );
        int idx_xCO2liq_0_15m = i++;       result.emplace_back( std::make_pair("xCO2liq_0_15m",0.0) );
        Scalar VolWeightedxCO2liq_5m = 0.0;
        Scalar VolWeightedxCO2liq_3m = 0.0;
        Scalar VolWeightedxCO2liq_0_15m = 0.0;

            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());
            for (i = 0; i < upperProbeRegionElmIdces_.size();i++)
            {
                const auto& element = this->gridGeometry().element(upperProbeRegionElmIdces_[i]);
                fvGeometry.bind(element);
                elemVolVars.bind(element, fvGeometry, x);
                for (const auto& scv : scvs(fvGeometry))
                {
                    VolWeightedxCO2liq_5m += elemVolVars[scv].moleFraction(0, 1)*elemVolVars[scv].extrusionFactor()*scv.volume();
                }
            }
            for ( i = 0; i < middleProbleRegionElmIdces_.size(); i++)
            {
                const auto& element = this->gridGeometry().element(middleProbleRegionElmIdces_[i]);
                fvGeometry.bind(element);
                elemVolVars.bind(element, fvGeometry, x);
                for (const auto& scv : scvs(fvGeometry))
                {
                    VolWeightedxCO2liq_3m += elemVolVars[scv].moleFraction(0,1)*elemVolVars[scv].extrusionFactor()*scv.volume();
                }
            }
            for( i = 0;i < lowerProbeRegionElmIdces_.size();i++)
            {
                const auto& element = this->gridGeometry().element(lowerProbeRegionElmIdces_[i]);
                fvGeometry.bind(element);
                elemVolVars.bind(element, fvGeometry, x);
                for (const auto& scv : scvs(fvGeometry))
                {
                    VolWeightedxCO2liq_0_15m += elemVolVars[scv].moleFraction(0, 1)*elemVolVars[scv].extrusionFactor()*scv.volume();
                }
            }
        result[idx_xCO2liq_5m].second = VolWeightedxCO2liq_5m/avgVol_5m_;
        result[idx_xCO2liq_3m].second = VolWeightedxCO2liq_3m/avgVol_3m_;
        result[idx_xCO2liq_0_15m].second = VolWeightedxCO2liq_0_15m/avgVol_0_15m_;
        return result;
    }

    void calculateAvgVols(const GridVariables& gridVars,
                                 const SolutionVector& x)
    {
        for( const auto& element : elements(this->gridGeometry().gridView()) )
        {
            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            const auto& globalPos = element.geometry().center();
            const unsigned int elemIdx = this->gridGeometry().elementMapper().index(element);
            if(globalPos[0] < 0.14 + eps_ && globalPos[0] > 0.09 - eps_)
            {
                if(globalPos[1] > 4.98-eps_ && globalPos[1] < 5.02 + eps_)
                {
                    upperProbeRegionElmIdces_.push_back(elemIdx);
                    for (const auto& scv : scvs(fvGeometry))
                    {
                        std::cout << "Submit volume to region of upper Probe. ElemIdx : " << elemIdx << " which adds "<< elemVolVars[scv].extrusionFactor()*scv.volume() << " cubic meters per meter"<<std::endl;
                        avgVol_5m_ += elemVolVars[scv].extrusionFactor()*scv.volume();
                    }
                }
                else if( globalPos[1] > 2.96-eps_ && globalPos[1] < 3.04+eps_)
                {
                    middleProbleRegionElmIdces_.push_back(elemIdx);
                    for (const auto& scv : scvs(fvGeometry))
                    {
                        std::cout << "Submit volume to region of middle Probe. ElemIdx : " << elemIdx << " which adds "<< elemVolVars[scv].extrusionFactor()*scv.volume() << " cubic meters per meter"<<std::endl;
                        avgVol_3m_ += elemVolVars[scv].extrusionFactor()*scv.volume();
                    }
                }
                else if(globalPos[1] > 0.13-eps_ && globalPos[1] < 0.17 + eps_)
                {
                    lowerProbeRegionElmIdces_.push_back(elemIdx);
                    for (const auto& scv : scvs(fvGeometry))
                    {
                        std::cout << "Submit volume to region of lower Probe. ElemIdx : " << elemIdx << " which adds "<< elemVolVars[scv].extrusionFactor()*scv.volume() << " cubic meters per meter"<<std::endl;
                        avgVol_0_15m_ += elemVolVars[scv].extrusionFactor()*scv.volume();
                    }
                }
            }
        }
    }
    template<class SubControlVolume>
    bool isLowerLeftCell_(const SubControlVolume& scv) const
    {
        return scv.dofIndex() == 0;
    }
    const std::string& name() const
    {
        return name_;
    }
private:


    bool onTopBoundary_(const GlobalPosition& globalPos) const
    { return globalPos[dimWorld-1] > this->gridGeometry().bBoxMax()[dimWorld-1] - eps_; }

    Scalar time() const
    {
        return timeLoop_->time();
    }
#if PULSES==1
        void assignPerturbationElements_()
        {
            const auto numElements = this->gridGeometry().gridView().size(0);
            perturbativeElements_.resize(numElements, false);
            const auto n_x = getParam<unsigned int>("Grid.Cells0");
            unsigned int i_y=0;
            unsigned int i_x=0;
            for (const auto& element : elements(this->gridGeometry().gridView()  ))
            {
                const auto& eIdx = this->gridGeometry().elementMapper().index(element);
                if(eIdx == i_y*n_x)
                {
                    perturbativeElements_[eIdx]=true;
                    std::cout << "assign "<< eIdx << "to perturbativeElements_; globalPos=" <<element.geometry().center()<<std::endl;
                }
                else if(eIdx==(i_y+1)*n_x-1)
                {
                    perturbativeElements_[eIdx]=true;
                    std::cout << "assign "<< eIdx << "to perturbativeElements_; globalPos="<<element.geometry().center()<<std::endl;
                }
                i_x++;
                if(i_x == n_x)
                {
                    i_y++;
                    i_x=0;
                }
            }
        }
#endif
    const Scalar eps_;

    std::string name_;
    std::vector<Scalar> deltaRho_;
    Scalar initialCO2aq_;
    TimeLoopPtr timeLoop_;
    Scalar storage_;
    Scalar temperatureInitial_;
    std::ofstream fileTotalMoles_;
    Scalar intitializationTime_;
    Scalar restartTime_;
    Scalar avgVol_5m_=0;
    Scalar avgVol_3m_=0;
    Scalar avgVol_0_15m_=0;
    std::string filenameTotalMolesCO2_;
    const std::vector<Dune::FieldVector<double, 3>>& co2Data_;
    mutable std::vector<Scalar> actualData_={0.0,0.0};
    mutable std::vector<unsigned int> upperProbeRegionElmIdces_, middleProbleRegionElmIdces_, lowerProbeRegionElmIdces_;
#if PULSES==1
    mutable std::vector<bool> perturbativeElements_;
    int tIdxCount_=12;
#endif
    bool perturb_=false;
    int timeIndex_;

};


} //end namespace

#endif
